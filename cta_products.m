function product_mat=cta_products(Fourier_coefficients,varargin)

if ~iscell(Fourier_coefficients)
    Fourier_coefficients={Fourier_coefficients};
end;

monoms={'01'};


feature_order=[0,5];
angular_power=[feature_order(1),feature_order(2)];
angular_cross=[1,2];

for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

if exist('output_order'),
    feature_order(1)=max(feature_order(1),output_order);
end;

assert(max(cellfun(@numel,monoms))<4);
assert(min(cellfun(@numel,monoms))>0);


num_features=numel(Fourier_coefficients);

product_mat=[];

for m=1:numel(monoms),
    morder=numel(monoms{m});
    monom=monoms{m};
    for a=1:numel(Fourier_coefficients)
        for la=0:Fourier_coefficients{a}.L
            if (morder==1)
                assert(str2double(monom(1))==0);
                   new_product=[a,la,str2double(monom(1)),...
                                 -1,-1,-1,...
                                 -1,-1,-1,...
                                 la,0];
                    product_mat=[product_mat;new_product];                        
            else            
                for b=a:numel(Fourier_coefficients)
                    start_bl=0;
                    if (a==b) && (monom(1)==monom(2))
                        start_bl=la;
                    end;
                    for lb=start_bl:Fourier_coefficients{b}.L
                       if (morder==2)
                            l=(-1)^str2double(monom(1))*la+(-1)^str2double(monom(2))*lb;

                            if (in_interval(l,feature_order)) && ...
                               ((a==b)&&(in_interval([la,lb],angular_power)) || ...
                               ((a~=b)&&in_interval([la,lb],angular_cross)))

                                new_product=[a,la,str2double(monom(1)),...
                                             b,lb,str2double(monom(2)),...
                                             -1,-1,-1,...
                                             l,0];
                                product_mat=[product_mat;new_product];
                            end;

                        elseif (morder==3)
                            for c=b:numel(Fourier_coefficients)
                                start_cl=0;
                                if (c==b) && (monom(2)==monom(3))
                                    start_cl=lb;
                                end;  
                                if (c==a) && (monom(1)==monom(3))
                                    start_cl=max(la,start_cl);
                                end;                                 
                                for lc=start_cl:Fourier_coefficients{c}.L
                                    %if min([la,lb,lc])>0
                                    if min([lb,lc])>0
                                    %if 1
                                        l=(-1)^str2double(monom(1))*la...
                                            +(-1)^str2double(monom(2))*lb...
                                            +(-1)^str2double(monom(3))*lc;

                                        if (in_interval(l,feature_order)) && ...
                                           ((a==b)&&(b==c)&&(a==c)&&(in_interval([la,lb,lc],angular_power)) || ...
                                           (((a~=b)||(b~=c)||(a~=c))&&in_interval([la,lb,lc],angular_cross)))

                                            new_product=[a,la,str2double(monom(1)),...
                                                         b,lb,str2double(monom(2)),...
                                                         c,lc,str2double(monom(3)),...
                                                         l,0];
                                            product_mat=[product_mat;new_product];
                                        end;
                                    end;
                                end;
                            end;
                        end;
                    end;
                end;
        end;
        end;
    end;
end;

[v,indx]=sort(product_mat(:,end-1),'descend');
product_mat=product_mat(indx,:);


function ok=in_interval(value,interval)
    ok=true;
    if numel(interval)<2
        ok=false;
    end;
    for v=1:numel(value)
        if (value(v)<(interval(1))) || (value(v)>(interval(2)))
            ok=false;
        end
    end;


