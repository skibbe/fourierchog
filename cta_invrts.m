% CTA_INVRTS computes rotation invariant features based on the circular
% Fourier HOG features returned by cta_chog
% 
%
% fimage=cta_chog(chog,...            % list of Fourier HOG coefficients
%               'product_options',...  
%               {'monoms',{'001'}});  % rules for combining the coefficients                
%                                     % 001 means three coefficients while 
%                                     % the third one is the complex conjugate   
%                                     % coefficient, e.g. 
%                                     % {'01','001','011'}
%                                       
% returns a feature image 
%
%     fimage            % FxNxM image containing the F feature images
%
% These features ARE rotation invariant
%
% See also cta_chog cta_train cta_apply


%
% Copyright (c) 2011, Henrik Skibbe and Marco Reisert
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met: 
% 
% 1. Redistributions of source code must retain the above copyright notice, this
%    list of conditions and the following disclaimer. 
% 2. Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution. 
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
% The views and conclusions contained in the software and documentation are those
% of the authors and should not be interpreted as representing official policies, 
% either expressed or implied, of the FreeBSD Project.
%


function [fimage,product_mat]=cta_invrts(chog,varargin)

if ~iscell(chog)
    chog={chog};
end;


%product_options={'monoms',{'01','001'}};
product_options={'monoms',{'001'}};

for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


product_mat=cta_products(chog,'feature_order',[0,0],product_options{:});

even=(product_mat(:,8)==-1)&(product_mat(:,1)==product_mat(:,4));
even=even|((0==min(product_mat(:,[2,5,8]),[],2)));

shape=chog{1}.shape;
num_products=size(product_mat,1);
assert(sum(0==product_mat(:,end-1))==num_products);

fdim=sum((even==0)*2+even);
fimage=zeros([fdim,shape],class(chog{1}.data));

count=1;
for p=1:num_products,
    product=product_mat(p,:);
    if product(3)
        A=conj(squeeze(chog{product(1)}.data(product(2)+1,:,:)));
    else
        A=(squeeze(chog{product(1)}.data(product(2)+1,:,:)));
    end;

    if product(6)
        B=conj(squeeze(chog{product(4)}.data(product(5)+1,:,:)));
    else
        B=(squeeze(chog{product(4)}.data(product(5)+1,:,:)));
    end;        
    
    if (product(7)==-1)
        if even(p)
            fimage(count,:,:)=A.*B;
            count=count+1;
        else
            tmp=A.*B;
            fimage(count,:,:)=real(tmp);
            count=count+1;        
            fimage(count,:,:)=imag(tmp);
            count=count+1;            
        end;
    else
        if product(9)
            C=conj(squeeze(chog{product(7)}.data(product(8)+1,:,:)));
        else
            C=(squeeze(chog{product(7)}.data(product(8)+1,:,:)));
        end;        
        if even(p)
            tmp=A.*B.*C;
            fimage(count,:,:)=real(tmp);
            count=count+1;        
        else
            tmp=A.*B.*C;
            fimage(count,:,:)=real(tmp);
            count=count+1;        
            fimage(count,:,:)=imag(tmp);
            count=count+1;            
        end;
    end;
end;



