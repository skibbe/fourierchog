% You can find us here:
% http://www.uniklinik-freiburg.de/mr/live/mitarbeiter/aktuelle/skibbe_en.html
% and the source-code here:
% https://bitbucket.org/skibbe/fourierchog/get/default.zip


%
% Copyright (c) 2011, Henrik Skibbe and Marco Reisert
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met: 
% 
% 1. Redistributions of source code must retain the above copyright notice, this
%    list of conditions and the following disclaimer. 
% 2. Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution. 
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
% The views and conclusions contained in the software and documentation are those
% of the authors and should not be interpreted as representing official policies, 
% either expressed or implied, of the FreeBSD Project.
%


% The following code for densely computing local Fourier HOG features
% is based on our paper: 
%
% Henrik Skibbe and Marco Reisert 
% "Circular Fourier-HOG Features for Rotation Invariant Object Detection in Biomedical Images"
% in Proceedings of the IEEE International Symposium on Biomedical Imaging 2012 (ISBI 2012), Barcelona 
%
% You can find a free copy here :
% http://skl220b.ukl.uni-freiburg.de/mr/authoring/mitarbeiter/aktuelle/skibbe/CHOGFilterPaper_en.pdf
%
% If you use our functions or partially make use of our code then please cite this paper.


%% Loading & initializing traing images

which_label=1;  % 1 or 2

absorption=0;    % Simulates absorption leading to a decreasing 
                 % signal/noise ratio in X direction 
noise=0.00;      % Noise level


train_img_fname='train_orient1.png';


label_positions={[29,18;28,36],[29,56;29,75]};  % object positions       
label_orientations={[(-1-1i)/sqrt(2),(-1+1i)/sqrt(2)],[-1,1i]};     % object orientations encoded in 
                                                                    % terms of complex numbers
label_symmetry_order={1,2};                                                                  ;
                                                                    


          
train_imgs={};      % List of training images
train_labels={};    % List of label images 
train_orientation_labels={};    % List of label images 

figure(1);


    img=1-double((imread(train_img_fname)))/255;
    
    label=zeros(size(img));
    indx=sub2ind(size(img),label_positions{which_label}(:,1),label_positions{which_label}(:,2));
    label(indx)=1;
    
    olabel=zeros(size(img));
    olabel(indx)=label_orientations{which_label};
            
    train_imgs{1}=cta_absorption(img,absorption,noise);
    train_labels{1}=label;
    train_orientation_labels{1}=olabel;
    
    % create plots
    imshow(real(train_imgs{1}.^0.5),'InitialMagnification','fit');
    [x,y,z] = cylinder([1,1],200);
    [x2,y2,z2] = cylinder([5,5],200);
    hold on;
    [gt_x,gt_y]=ind2sub(size(train_labels{1}),find(train_labels{1}==1));       
    for d=1:numel(gt_x)
            plot(x2(1,:)+gt_y(d),y2(1,:)+gt_x(d),'g','LineWidth',2);
            plot(x(1,:)+gt_y(d),y(1,:)+gt_x(d),'r','LineWidth',2);
            plot([gt_y(d),gt_y(d)+real(label_orientations{which_label}(d))*10],[gt_x(d),gt_x(d)+imag(label_orientations{which_label}(d))*10],'b','LineWidth',3);
    end;
    

    
    xlabel(['Training image ',num2str(1),' with label positions']);

    



%% Parameter setup 
                

% Maximum angular frequency for the Fourier HOG features and the filter
L=5;

% Size of the voting function of the Harmonic Filter 
% (complex Gaussian derivatives)
v_sigma=2;

% Non-linear features fpr the filter.
% Here: multiplication of three Fourier coefficients while the third one 
% is the complex conjugate coefficient
product_options={'monoms',{'001'}};                

% We choose Fourier HOG features with 4 radial bins
% They should cope with the differnt kinds of shapes 
% in the training images
w_func={'circle',[0,2],[2,2],[4,2]};


% Size of the voting function of the Harmonic Filter (orientation field)
% (complex Gaussian derivatives)
v_sigma2=2;

% We choose Fourier HOG features with 1 Gaussian shaped bin (orientation field)
w_func2={'gauss',2};


% Additional options for the Fourier HOG features
chog_options={  'w_func',w_func,...
                'presmooth',2.0,... % initial smoothing (before computing the image gradient)
                'l2',true,... % in order to cope with the absorption we choose l2 normalization
                'gamma',0.8,... % makes the gradient orientation more dominant over magnitude
                'verbosity',0}; %  plot the window functions 1=yes/0=no


              
fprintf('... we train a filter for the orientation detection task\n');                
fprintf('training takes a while, because we must build the whole system of equations & solve it\n');                

tic            
[model_orient,fimage]=cta_train2(train_imgs,...     % List of training images
                         train_orientation_labels,...   % List of training label direction images
                         'precision','double',...   % We choose double precision numbers
                         'w_func',w_func2,...
                         'v_sigma',v_sigma2,...
                         'product_options',product_options,...
                         'L',L,...
                        'chog_options',chog_options,...
                        'verbosity',1,...
                        'output_order',label_symmetry_order{which_label}); % a gradient is of order 1
toc                    
                    




%%  Loading & initializing the test images

test_img_fnames={'test_orient1.png'};


test_images={};

for l=1:numel(test_img_fnames)
    img=1-double((imread(test_img_fnames{l})))/255;
    img=cta_absorption(img,absorption,noise);

   
    test_images{l}=img;
    
    figure(10);
    subplot(1,numel(test_img_fnames),l);
    imshow(real(img.^0.5),'InitialMagnification','fit');
    xlabel(['Test image ',num2str(l)]);
end;
   



%% Applying the filtersto the test image

    for l=1:numel(test_img_fnames)
        fprintf('processing test image %d\n',l);
        tic
        % orientations
        Horientation{l}=cta_apply2(test_images{l},model_orient,'padding',0,'precision','single');
        % detections
        H{l}=abs(Horientation{l});
        toc
    end;


%% Local maxima detection
    for l=1:numel(test_img_fnames)
        Hdet{l} = (H{l}.*(H{l} > imdilate(H{l}, [1 1 1; 1 0 1; 1 1 1])))>0.55;
        [test_gt(l).det_x,test_gt(l).det_y]=ind2sub(size(Hdet{l}),find(Hdet{l}==1));
    end;

%% Plotting results (dtections + orientations)
for l=1:numel(test_img_fnames)
   
    figure(l*1000+1);
    clf;
    subplot(1,1,1)
    imshow(H{l},'InitialMagnification','fit');
    colormap jet
    colorbar 
    xlabel(['Filter response (test image ',num2str(l),')']);
    axis image;
    axis on;
   

    figure(l*1000+2);
    clf;
    subplot(1,1,1)
    RGB=zeros([size(test_images{l}),3]);
    RGB(:,:,1)=test_images{l};
    H2=H{l}./max(H{l}(:));RGB(:,:,2)=H2.*(H2>0);
    imshow(RGB,'InitialMagnification','fit');
    xlabel(['Image & filter response (test image ',num2str(l),')']);
    axis on;
   

    [x,y,z] = cylinder([3,3],200);
    figure(l*1000+3);
    clf;
    subplot(1,1,1)
    imshow(real(test_images{l}.^0.5),'InitialMagnification','fit');
    hold on;

    for d=1:numel(test_gt(l).det_x)
            plot(x(1,:)+test_gt(l).det_y(d),y(1,:)+test_gt(l).det_x(d),'r','LineWidth',2);
    end;


    orient_img=Horientation{l};
    orient_img2=orient_img./(abs(orient_img)+eps);
    direction_x=real(orient_img2);
    direction_y=imag(orient_img2);
    for d=1:numel(test_gt(l).det_x)
        py=test_gt(l).det_y(d);
        px=test_gt(l).det_x(d);
        plot([py,py+direction_x(px,py)*10],[px,px+direction_y(px,py)*10],'b','LineWidth',3);
    end;

    
[X,Y]=meshgrid(1:2:size(orient_img,2),1:2:size(orient_img,1));
quiver(X,Y,real(orient_img(1:2:end,1:2:end)),imag(orient_img(1:2:end,1:2:end)),'r','linewidth',1,'AutoScale','off');    
    

    xlabel(['Red: Detections after thresholding (image ',num2str(l),')']);
    axis on;
    
   
   
end;




