% You can find us here:
% http://www.uniklinik-freiburg.de/mr/live/mitarbeiter/aktuelle/skibbe_en.html
% and the source-code here:
% https://bitbucket.org/skibbe/fourierchog/get/default.zip


%
% Copyright (c) 2011, Henrik Skibbe and Marco Reisert
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met: 
% 
% 1. Redistributions of source code must retain the above copyright notice, this
%    list of conditions and the following disclaimer. 
% 2. Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution. 
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
% The views and conclusions contained in the software and documentation are those
% of the authors and should not be interpreted as representing official policies, 
% either expressed or implied, of the FreeBSD Project.
%


% The following code for densely computing local Fourier HOG features
% is based on our paper: 
%
% Henrik Skibbe and Marco Reisert 
% "Circular Fourier-HOG Features for Rotation Invariant Object Detection in Biomedical Images"
% in Proceedings of the IEEE International Symposium on Biomedical Imaging 2012 (ISBI 2012), Barcelona 
%
% You can find a free copy here :
% http://skl220b.ukl.uni-freiburg.de/mr/authoring/mitarbeiter/aktuelle/skibbe/CHOGFilterPaper_en.pdf
%
% If you use our functions or partially make use of our code then please cite this paper.


%% Loading & initializing traing images


which_label=3;   % Valid labels are 1,2,3,4,5,6

absorption=1;    % Simulates absorption leading to a decreasing 
                 % signal/noise ratio in X direction 
noise=0.02;      % Noise level


train_img_fnames={'strain_img.png','strain_img2.png'};
train_labels_fnames={'strain_labels.png','strain_labels2.png'};




label_colors=[0,255,0;
              255,0,0;
              0,255,255;
              255,255,0;
              255,0,255;
              0,0,255];

          
train_imgs={};      % List of training images
train_labels={};    % List of label images 

figure(1);
for l=1:numel(train_img_fnames),
    img=1-double((imread(train_img_fnames{l})))/255;
    labels=double(imread(train_labels_fnames{l}));
    
    
    label=bwmorph((labels(:,:,1)==label_colors(which_label,1))&...
                    (labels(:,:,2)==label_colors(which_label,2))&...
                    (labels(:,:,3)==label_colors(which_label,3)),'shrink',Inf);
          
                
    train_imgs{l}=cta_absorption(img,absorption,noise);
    train_labels{l}=label;
    
    % create plots
    subplot(1,numel(train_img_fnames),l);
    imshow(real(train_imgs{l}.^0.5),'InitialMagnification','fit');
    [x,y,z] = cylinder([1,1],200);
    [x2,y2,z2] = cylinder([5,5],200);
    hold on;
    [gt_x,gt_y]=ind2sub(size(train_labels{l}),find(train_labels{l}==1));       
    for d=1:numel(gt_x)
            plot(x2(1,:)+gt_y(d),y2(1,:)+gt_x(d),'g','LineWidth',2);
            plot(x(1,:)+gt_y(d),y(1,:)+gt_x(d),'r','LineWidth',2);
    end;
    xlabel(['Training image ',num2str(l),' with label positions']);
end;
    



%% Parameter setup 
                

% Maximum angular frequency for the Fourier HOG features and the filter
L=5;

% Size of the voting function of the Harmonic Filter 
% (complex Gaussian derivatives)
v_sigma=6;

% Non-linear features fpr the filter.
% Here: multiplication of three Fourier coefficients while the third one 
% is the complex conjugate coefficient
product_options={'monoms',{'001'},'feature_order',[0,5]};                

% We choose Fourier HOG features with 4 radial bins
% They should cope with the differnt kinds of shapes 
% in the training images
w_func={'circle',[0,2],[2,2],[4,2],[6,2]};

% Additional options for the Fourier HOG features
chog_options={  'w_func',w_func,...
                'presmooth',2.0,... % initial smoothing (before computing the image gradient)
                'l2',true,... % in order to cope with the absorption we choose l2 normalization
                'gamma',0.8,... % makes the gradient orientation more dominant over magnitude
                'verbosity',0}; %  plot the window functions 1=yes/0=no

       
fprintf('training takes a while, because we must build the whole system of equations & solve it\n');                
tic            
[model,fimage]=cta_train(train_imgs,...             % List of training images
                         train_labels,...           % List of training label images
                         'precision','double',...   % We choose double precision numbers
                         'w_func',w_func,...
                         'v_sigma',v_sigma,...
                         'product_options',product_options,...
                         'L',L,...
                        'chog_options',chog_options,...
                        'verbosity',1);
                    
toc  



%%  Loading & initializing the test images

test_img_fnames={'stest_img.png','stest_img2.png'};
test_labels_fnames={'stest_labels.png','stest_labels2.png'};


test_images={};
clear test_gt;

for l=1:numel(test_img_fnames)
    img=1-double((imread(test_img_fnames{l})))/255;
    img=cta_absorption(img,absorption,noise);

    labels=double(imread(test_labels_fnames{l}));
    labels=bwmorph((labels(:,:,1)==label_colors(which_label,1))&...
                   (labels(:,:,2)==label_colors(which_label,2))&...
                   (labels(:,:,3)==label_colors(which_label,3)),'shrink',Inf);
                    
                
    [test_gt(l).gt_x,test_gt(l).gt_y]=ind2sub(size(labels),find(labels==1));  
    
    test_images{l}=img;
    
    figure(10);
    subplot(1,numel(test_img_fnames),l);
    imshow(real(img.^0.5),'InitialMagnification','fit');
    xlabel(['Test image ',num2str(l)]);
end;
   



%% Applying the filter to the test image

    for l=1:numel(test_img_fnames)
        fprintf('processing test image %d\n',l);
        tic
        H{l}=cta_apply(test_images{l},model,'padding',0,'precision','single');
        toc
    end;


%% Local maxima detection
    for l=1:numel(test_img_fnames)
        Hdet{l} = (H{l}.*(H{l} > imdilate(H{l}, [1 1 1; 1 0 1; 1 1 1])))>0.55;
        [test_gt(l).det_x,test_gt(l).det_y]=ind2sub(size(Hdet{l}),find(Hdet{l}==1));
    end;

%% Plotting results
for l=1:numel(test_img_fnames)
   
    figure(l*1000+1);
    clf;
    subplot(1,1,1)
    imshow(H{l},'InitialMagnification','fit');
    colormap jet
    colorbar 
    xlabel(['Filter response (test image ',num2str(l),')']);
    axis image;
    axis on;
   

    figure(l*1000+2);
    clf;
    subplot(1,1,1)
    RGB=zeros([size(test_images{l}),3]);
    RGB(:,:,1)=test_images{l};
    H2=H{l}./max(H{l}(:));RGB(:,:,2)=H2.*(H2>0);
    imshow(RGB,'InitialMagnification','fit');
    xlabel(['Image & filter response (test image ',num2str(l),')']);
    axis on;
   

    [x,y,z] = cylinder([1,1],200);
    [x2,y2,z2] = cylinder([5,5],200);
    figure(l*1000+3);
    clf;
    subplot(1,1,1)
    imshow(real(test_images{l}.^0.5),'InitialMagnification','fit');
    %image(real(test_images{l}.^0.5));
    hold on;
    for d=1:numel(test_gt(l).gt_x)
            plot(x2(1,:)+test_gt(l).gt_y(d),y2(1,:)+test_gt(l).gt_x(d),'g','LineWidth',2);
    end;
    for d=1:numel(test_gt(l).det_x)
            plot(x(1,:)+test_gt(l).det_y(d),y(1,:)+test_gt(l).det_x(d),'r','LineWidth',2);
    end;
    xlabel(['Red: Detections after thresholding /  Green: ground truth (image ',num2str(l),')']);
    axis on;
   
end;




