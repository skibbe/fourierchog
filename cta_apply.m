% CTA_APPLY appliction of the Fourier CHOG filter described in  
%
% Henrik Skibbe and Marco Reisert 
% "Circular Fourier-HOG Features for Rotation Invariant Object Detection in Biomedical Images"
% in Proceedings of the IEEE International Symposium on Biomedical Imaging 2012 (ISBI 2012), Barcelona 
%
% H=cta_apply(image,...                    % NxM image
%             model,...                    % the filter model (see cta_train)
%             'padding',0);                % 0-border padding 
%                                          % (here 0 pixels -> disabled)
%
%
% returns a filter model. Use the model in combinaion with cta_apply to
% apply the trained filter to new images
%
%
% See also cta_chog cta_invrts cta_train 


%
% Copyright (c) 2011, Henrik Skibbe and Marco Reisert
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are met: 
% 
% 1. Redistributions of source code must retain the above copyright notice, this
%    list of conditions and the following disclaimer. 
% 2. Redistributions in binary form must reproduce the above copyright notice,
%    this list of conditions and the following disclaimer in the documentation
%    and/or other materials provided with the distribution. 
% 
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
% WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
% ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
% 
% The views and conclusions contained in the software and documentation are those
% of the authors and should not be interpreted as representing official policies, 
% either expressed or implied, of the FreeBSD Project.
%



function H = cta_apply(image,model,varargin)

precision='double';
verbosity=0;
padding=0;

for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;

shape=size(image);

if padding>0
    padded_shape=cta_fft_bestshape(shape+padding);
    original_shape=shape;
    img=zeros(padded_shape);
    img(1:shape(1),1:shape(2))=image;
    image=img;
end;


shape=size(image);


complex_derivatives=...
           [0,0,(-1i)/8,0,0;
           0,0,1i,0,0;
           (-1)/8,1 ,0,-1, (1)/8 ;
           0,0,-1i,0,0;
           0,0,(1i)/8,0,0;];
       
complex_derivatives=conj(complex_derivatives);  

    


chog=cta_chog(image,'L',model.L,'precision',precision,model.chog_options{:});

num_products=size(model.products,1);


H=zeros(shape,precision);



for vf=1:numel(model.v_sigma),
    L=model.products(1,end-1);
    H_tmp=zeros(shape,precision);
    for p=1:num_products,
        product=model.products(p,:);
        if product(3)
            A=conj(squeeze(chog{product(1)}.data(product(2)+1,:,:)));
        else
            A=(squeeze(chog{product(1)}.data(product(2)+1,:,:)));
        end;



        if (product(4)==-1)
             if verbosity>1
                fprintf('(%d) [%d]%d  -> %d\n',product(1),(-1)^product(3),product(2),product(2));
             end;
                tmp=A;
        else
            if product(6)
                B=conj(squeeze(chog{product(4)}.data(product(5)+1,:,:)));
            else
                B=(squeeze(chog{product(4)}.data(product(5)+1,:,:)));
            end;  

            if (product(7)==-1)
                 if verbosity>1
                    fprintf('(%d) [%d]%d x (%d) [%d]%d -> %d\n',product(1),(-1)^product(3),product(2),product(4),(-1)^product(6),product(5),product(10));
                 end;
                    tmp=A.*B;
            else
                if product(9)
                    C=conj(squeeze(chog{product(7)}.data(product(8)+1,:,:)));
                else
                    C=(squeeze(chog{product(7)}.data(product(8)+1,:,:)));
                end;
                 if verbosity>1
                    fprintf('(%d) [%d]%d x (%d) [%d]%d x (%d) [%d]%d -> %d\n',product(1),(-1)^product(3),product(2),product(4),(-1)^product(6),product(5),product(7),(-1)^product(9),product(8),product(10));
                 end;
                    tmp=A.*B.*C;
            end;
        end;
        l=product(end-1);
        while (l<L)
            L=L-1;
            H_tmp=imfilter(H_tmp,complex_derivatives,model.filter_mode);
        end;
        H_tmp=H_tmp+cast(model.alpha{vf}(p),precision)*tmp;
    end;
    
     while (L>0)
        L=L-1;
        H_tmp=imfilter(H_tmp,complex_derivatives,model.filter_mode);
    end;
    
    ft_kernel=fftn(cta_fspecial('gauss',model.v_sigma(vf),shape,false,precision));
    H=H+real(ifftn(fftn(H_tmp).*ft_kernel));
end;




if padding>0
    H=H(1:original_shape(1),1:original_shape(2));
end;

Mask=zeros(size(H));
border=ceil(max(model.v_sigma));
Mask(border:end-border+1,border:end-border+1)=1;
H(Mask==0)=0;











